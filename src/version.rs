use git_testament::{GitModification, GitTestament};

#[derive(Debug, thiserror::Error)]
pub enum VersionError {
    #[error("string formatting error: {0}")]
    Format(#[from] std::fmt::Error),
}

pub struct Version<'a> {
    testament: &'a GitTestament<'a>,
}

impl<'a> Version<'a> {
    pub fn new(testament: &'a GitTestament<'a>) -> Self {
        Self { testament }
    }

    pub fn version(&self) -> Result<String, VersionError> {
        use std::fmt::Write as _;
        let mut ret = String::new();
        writeln!(ret, "{}", &self.testament)?;
        writeln!(
            ret,
            "{} {}",
            env!("CARGO_PKG_NAME"),
            env!("CARGO_PKG_VERSION")
        )?;
        Ok(ret)
    }

    pub fn long_version(&self) -> Result<String, VersionError> {
        use std::fmt::Write as _;
        let mut ret = String::new();
        writeln!(ret, "{}", &self.testament)?;
        writeln!(ret, "Crate version: {}", env!("CARGO_PKG_VERSION"))?;
        if let Some(branch) = self.testament.branch_name {
            writeln!(ret, "Built from branch: {}", branch)?;
        } else {
            writeln!(ret, "Branch information is missing.")?;
        }
        writeln!(ret, "Commit info: {}", self.testament.commit)?;
        if self.testament.modifications.is_empty() {
            writeln!(ret, "Working tree is clean")?;
        } else {
            use GitModification::*;
            for fmod in self.testament.modifications {
                match fmod {
                    Added(f) => writeln!(ret, "Added: {}", String::from_utf8_lossy(f))?,
                    Removed(f) => writeln!(ret, "Removed: {}", String::from_utf8_lossy(f))?,
                    Modified(f) => writeln!(ret, "Modified: {}", String::from_utf8_lossy(f))?,
                    Untracked(f) => writeln!(ret, "Untracked: {}", String::from_utf8_lossy(f))?,
                }
            }
        }
        Ok(ret)
    }
}
