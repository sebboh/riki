use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;
use log::debug;

#[derive(Debug, Eq, PartialEq)]
pub struct Table {
    data: String,
}

impl DirectiveImplementation for Table {
    const REQUIRED: &'static [&'static str] = &["data"];
    const ALLOWED: &'static [&'static str] = &[];
    const ALLOW_ANY_UNNAMED: bool = true;

    fn from_parsed(p: &ParsedDirective) -> Self {
        Self::new(p.args().get("data").unwrap().to_string())
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        let mut table = String::new();
        let mut lines = self.data.trim().lines();
        if let Some(first) = lines.next() {
            let columns = first.split('|').count();
            debug!("table first: {:?}", first);
            debug!("table column count: {}", columns);
            table.push_str(&format!("|{}|\n", first));
            for _ in 0..columns {
                table.push_str("|-");
            }
            table.push_str("|\n");
        }
        for line in lines {
            table.push_str(&format!("|{}|\n", line));
        }
        debug!("table data: {}", self.data);
        debug!("table: {}", table);
        Ok(Processed::Markdown(table))
    }
}

impl Table {
    pub fn new(data: String) -> Self {
        Self { data }
    }
}
