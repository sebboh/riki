use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;
use log::warn;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct Brokenlinks {}

impl DirectiveImplementation for Brokenlinks {
    const REQUIRED: &'static [&'static str] = &["pages"];
    const ALLOWED: &'static [&'static str] = &[];
    const ALLOW_ANY_UNNAMED: bool = false;

    fn from_parsed(_: &ParsedDirective) -> Self {
        Self::default()
    }

    fn process(&self, _site: &Site, meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        warn!(
            "page {} uses unimplemented brokenlinks",
            meta.path().display()
        );
        Ok(Processed::Markdown("\n".into()))
    }
}
