use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::{Shortcut as S, Site};
use crate::wikitext::ParsedDirective;

use log::trace;

#[derive(Debug, Eq, PartialEq)]
pub struct Shortcut {
    shortcut: S,
}

impl DirectiveImplementation for Shortcut {
    const REQUIRED: &'static [&'static str] = &["name", "url"];
    const ALLOWED: &'static [&'static str] = &["desc"];
    const ALLOW_ANY_UNNAMED: bool = false;

    fn from_parsed(p: &ParsedDirective) -> Self {
        let args = p.args();
        let name = args.get("name").unwrap();
        let desc = args.get("desc").unwrap_or(&"");
        let url = args.get("url").unwrap();
        Self::new(S::new(name, desc, url))
    }

    fn prepare(&self, site: &mut Site) -> Result<(), DirectiveError> {
        trace!("shortcut: prepare");
        site.add_shortcut(self.shortcut.clone());
        Ok(())
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        Ok(Processed::Markdown("".into()))
    }
}

impl Shortcut {
    pub fn new(shortcut: S) -> Self {
        Self { shortcut }
    }
}
