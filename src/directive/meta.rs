use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::time::parse_timestamp;
use crate::wikitext::ParsedDirective;

use std::path::PathBuf;

#[derive(Default, Debug, Eq, PartialEq)]
pub struct Meta {
    title: Option<String>,
    date: Option<String>,
    link: PathBuf,
}

impl DirectiveImplementation for Meta {
    const REQUIRED: &'static [&'static str] = &[];
    const ALLOWED: &'static [&'static str] = &["date", "link", "title", "author"];
    const ALLOW_ANY_UNNAMED: bool = false;

    fn from_parsed(p: &ParsedDirective) -> Self {
        let mut meta = Self::default();
        let args = p.args();
        if let Some(title) = args.get("title") {
            meta.set_title(title);
        }
        if let Some(date) = args.get("date") {
            meta.set_date(date);
        }
        if let Some(link) = args.get("link") {
            meta.link = PathBuf::from(link);
        }
        meta
    }

    fn process(&self, _site: &Site, meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        if let Some(title) = &self.title {
            meta.set_title(title.into());
        }
        if let Some(mtime) = &self.date {
            meta.set_mtime(parse_timestamp(mtime)?);
        }
        meta.add_link(&self.link);
        Ok(Processed::Markdown("".into()))
    }
}

impl Meta {
    fn set_date(&mut self, date: &str) {
        self.date = Some(date.into());
    }

    fn set_title(&mut self, title: &str) {
        self.title = Some(title.into());
    }
}
