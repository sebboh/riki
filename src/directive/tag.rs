use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;

#[derive(Debug, Eq, PartialEq)]
pub struct Tag {
    tags: Vec<String>,
}

impl DirectiveImplementation for Tag {
    const REQUIRED: &'static [&'static str] = &[];
    const ALLOWED: &'static [&'static str] = &["class"];
    const ALLOW_ANY_UNNAMED: bool = true;

    fn from_parsed(p: &ParsedDirective) -> Self {
        let tags = p.unnamed_args().iter().map(|s| s.to_string()).collect();
        Tag::new(tags)
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        Ok(Processed::Markdown("".into()))
    }
}

impl Tag {
    pub fn new(tags: Vec<String>) -> Self {
        Self { tags }
    }
}
