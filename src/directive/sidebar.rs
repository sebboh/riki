use crate::directive::{DirectiveError, DirectiveImplementation, Processed};
use crate::page::PageMeta;
use crate::site::Site;
use crate::wikitext::ParsedDirective;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct Sidebar {}

impl DirectiveImplementation for Sidebar {
    const REQUIRED: &'static [&'static str] = &[];
    const ALLOWED: &'static [&'static str] = &["content"];
    const ALLOW_ANY_UNNAMED: bool = true;

    fn from_parsed(_: &ParsedDirective) -> Self {
        Self::default()
    }

    fn process(&self, _site: &Site, _meta: &mut PageMeta) -> Result<Processed, DirectiveError> {
        Err(DirectiveError::UnimplementedDirective("sidebar".into()))
    }
}
