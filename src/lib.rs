//! A static web site generator similar to ikiwiki
//!
//! [ikiwiki]: http://ikiwiki.info/
//!
//! ikiwiki is a "wiki compiler" that can also be used as a static
//! site generator. It's written mostly in Perl, and works well, if a
//! little slow. This care implements a subset of the functionality of
//! ikiwiki in Rust, for speed.

pub mod directive;
pub mod error;
pub mod git;
pub mod html;
pub mod name;
pub mod page;
pub mod pagespec;
pub mod parser;
pub mod site;
pub mod srcdir;
pub mod time;
pub mod token;
pub mod util;
pub mod wikitext;
pub mod version;
