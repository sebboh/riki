fn main() {
    println!("cargo:rerun-if-changed=src/pagespec.lalrpop");
    println!("building parser with larlpop");
    lalrpop::process_root().unwrap();
    println!("built parser with larlpop successfully");
    println!("generating code with Subplot");
    if let Err(e) = subplot_build::codegen("riki.subplot") {
        eprintln!("failed to generate code from subplot: {e}");
        std::process::exit(1);
    }
    println!("generated code with Subplot successfully");
}
